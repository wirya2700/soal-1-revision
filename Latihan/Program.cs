﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Latihan
{
    class Program
    {

        static void HitungBintang(Int32 jumlahBintang)
        {
            Int32 totalBintang = 0;
            for (int i = 1; i <= jumlahBintang; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write("*");
                    totalBintang += 1;
                }
                Console.WriteLine();

            }
            Console.WriteLine("Total bintang kamu : " + totalBintang);
            Pertanyaan();
            Console.ReadKey();
        }


        static void Pertanyaan()
        {
            Int32 jumlahBintang;
            Console.WriteLine("Masukan jumlah bintang :");
            jumlahBintang = Convert.ToInt32(Console.ReadLine());
            HitungBintang(jumlahBintang);

        }

        static void Main(string[] args)
        {
            Pertanyaan();
        }
    }
}
